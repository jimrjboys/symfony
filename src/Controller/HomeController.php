<?php

namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use symfony\Bundle\FrameworkBundle\Controller\Controller ;

  class HomeController extends Controller{

    public function hello($prenom = "anonyme"){

        /**
         * @Route("/Hello/{prenom}/age/{age}",name="hello")
         * @Route("/hello")
         * @Route("/hello/{prenom}")
         * @Route("hello/{prenom}" ,name="hello_prenom")
         * 
         * Montre la page qui dit bonjour
         */
        return $this->render(
            'hello.html.twig',[

                'prenom' => $prenom,
                'age' => $age
            
        ]);
    }
    /**
     * @Route("/",name="homepage")
     */
    public function home(){
         $prenom = ["Lior" => 31 , "Joseph" => 12 ,"Anne" => 55];
        return $this->render(
            'home.html.twig',
            [
                 'title'=>"Bonjour à tous",
                 'age' => 12 ,
                 'tableau' => $prenoms
            ]
        );


    }

  }

?>